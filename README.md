# CarCar

Team:

* Daniel Gettinger - service
* Adriel Stokes - sales

## Instructions to run the project:
Make sure docker, git, and Node.js 18.2 are installed and up-to-date.

1. Fork this repository

2. Clone the forked repository onto your local computer:
git clone https://gitlab.com/Dgett/project-beta

3. Build and run the project using Docker with these commands:
```
docker volume create beta-data
docker-compose build
docker-compose up
```
- After running these commands, make sure all of your Docker containers are running

- View the project in the browser: http://localhost:3000/

![Img](/images/CarCarWebsite.png)

## Design/Diagram:

Sales                           Inventory                   Service
 _______________                _____________              _____________
|-AutomobileVO |               |             |            |-Technician  |
|-Salesperson  |  <----------  |-Manufacturer| ---------> |-AutomobileVO|
|-Customer     |               |-VehicleModel|            |-Appointment |
|-Sale         |               |-Automobile  |            |             |
|______________|               |_____________|            |_____________|
Look at ./diagram.png for an Excalidraw diagram.


## API Documentation:

High Level Overview:
There are three main API's for this project: Service, Sales, and Inventory. These three microservices together
employ domain driven design and are designed to be able to stand on their own independently, but also to work in
unison. Sales and Service use pollers to pull the automobile information from the inventory microservice, allowing
them to keep up to date information about which cars have been sold or are being worked on. All three microservices
utalize their own ports and paths.



Inventory URLs, ports, and JSON bodys:
URLs and Ports back-end:

| Action | Method | URL
| ----------- | ----------- | ----------- |

| List manufacturers | GET | http://localhost:8100/api/manufacturers/
| Create a manufacturer | POST | http://localhost:8100/api/manufacturers/ |
| Get a specific manufacturer | GET | http://localhost:8100/api/manufacturers/id/
| Update a specific manufacturer | PUT | http://localhost:8100/api/manufacturers/id/
| Delete a specific manufacturer | DELETE | http://localhost:8100/api/manufacturers/id/

| List vehicle models | GET | http://localhost:8100/api/models/
| Create a vehicle model | POST | http://localhost:8100/api/models/
| Get a specific vehicle model | GET | http://localhost:8100/api/models/id/
| Update a specific vehicle model | PUT | http://localhost:8100/api/models/id/
| Delete a specific vehicle model | DELETE | http://localhost:8100/api/models/id/

- The **'vin'** at the end of the detail urls represents the VIN for the specific automobile you want to access. This is not an integer ID. This is a string value so you can use numbers and/or letters.

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List automobiles | GET | http://localhost:8100/api/automobiles/
| Create an automobile | POST | http://localhost:8100/api/automobiles/
| Get a specific automobile | GET | http://localhost:8100/api/automobiles/vin/
| Update a specific automobile | PUT | http://localhost:8100/api/automobiles/vin/
| Delete a specific automobile | DELETE | http://localhost:8100/api/automobiles/vin/



URLs and Ports front-end:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List manufacturers | GET | http://localhost:3000/manufacturers/
| List vehicle models | GET | http://localhost:3000/vehicle_model_list/
| Create manufacturer | POST | http://localhost:3000/manufacturers/new/
| Create vehicle model | POST http://localhost:3000/vehicle_model/new/
| List automobiles | GET | http://localhost:3000/inventory/automobiles/
| Create automobiles | POST | http://localhost:3000/inventory/automobiles/new/


Inventory JSON:

Create a manufacturer:
{
	"name": "Tesla"
}
List manufacturers/returned body:
{
	"manufacturers": [
		{
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Honda"
		},
		{
			"href": "/api/manufacturers/2/",
			"id": 2,
			"name": "Ford"
		},
		{
			"href": "/api/manufacturers/3/",
			"id": 3,
			"name": "Tesla"
		}
	]
}

Create vehicle model:
{
	"name": "TeslaModelY",
	"picture_url": "https://www.motortrend.com/uploads/2023/10/029-2023-tesla-model-y.jpg?fit=around%7C875:492",
	"manufacturer_id": 3
}
List vehicel models/returned JSON body:
{
	"href": "/api/models/5/",
	"id": 5,
	"name": "TeslaModelY",
	"picture_url": "https://www.motortrend.com/uploads/2023/10/029-2023-tesla-model-y.jpg?fit=around%7C875:492",
	"manufacturer": {
		"href": "/api/manufacturers/3/",
		"id": 3,
		"name": "Tesla"
	}
}

Create an automobile:
{
	"color": "White",
	"year": "2021",
	"vin": "12dfaeee887",
	"sold": "False",
	"model_id": 1
}
List automobiles/returned body:
{
	"href": "/api/automobiles/12dfaeee887/",
	"id": 13,
	"color": "White",
	"year": "2021",
	"vin": "12dfaeee887",
	"model": {
		"href": "/api/models/1/",
		"id": 1,
		"name": "Civic",
		"picture_url": "https://file.kelleybluebookimages.com/kbb/base/evox/CP/15455/2021-Honda-Civic-front_15455_032_1984x763_BS_cropped.png",
		"manufacturer": {
			"href": "/api/manufacturers/1/",
			"id": 1,
			"name": "Honda"
		}
	},
	"sold": "False"
}



### Sales Ports, URLs, JSON bodys
Back end URLs and Ports:
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List salespeople | GET | http://localhost:8090/api/salespeople/
| Create a salesperson | POST | http://localhost:8090/api/salespeople/
| Delete a salesperson | DELETE | http://localhost:8090/api/salesperson/:id/

| List customers | GET | http://localhost:8090/api/customers/
| Create a customer | GET | http://localhost:8090/api/customers
| Delete a specific customer | DELETE | http://localhost:8090/api/customers/:id/

- the id value to show a salesperson's salesrecord is the **"id" value tied to a salesperson.**

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List all sales | GET | http://localhost:8090/api/sales/
| Create a new sale | POST | http://localhost:8090/api/sales/
| Show salesperson's sales | GET | http://localhost:8090/api/sales/:id/
| Delete a salesperson's sale | GET | http://localhost:8090/api/sales/:id/


Sales front end routes:
| Action | Method | URL
| ----------- | ----------- | ----------- |
Add a salesperson: |POST| http://localhost:3000/salesperson/new/
Add a customer: |POST|  http://localhost:3000/customer/new/
Record a new sale: |POST| http://localhost:3000/sales/new/
Salespeoplle list: |GET| http://localhost:3000/salespersonlist/
Customer list: |GET| http://localhost:3000/customerlist/
List all sales: |GET| http://localhost:3000/saleslist/

Sales JSON bodys:
Create a salesperson:
{
	"first_name": "Thomas",
	"last_name": "Rawson",
	"employee_id": 67
}
List salespeople/returned body:
{
	"salesperson": [
		{
			"first_name": "Karl",
			"last_name": "Robertz",
			"employee_id": "63",
			"id": 5
		},
		{
			"first_name": "Frodo",
			"last_name": "Hubbard",
			"employee_id": "32",
			"id": 6
		},
		{
			"first_name": "Thomas",
			"last_name": "Rawson",
			"employee_id": "67",
			"id": 7
		}
	]
}


Create Customer
{
	"first_name": "Batman",
	"last_name": "Wayne",
	"phone_number": "8789876666",
	"address": "500 Clay St"
}
List Customers/returned body:
{
	"customer": [
		{
			"first_name": "Henry",
			"last_name": "Zabrowski",
			"phone_number": "7099897654",
			"address": "400 Brick St",
			"id": 6
		},
		{
			"first_name": "Batman",
			"last_name": "Wayne",
			"phone_number": "8789876666",
			"address": "500 Clay St",
			"id": 8
		},
		{
			"first_name": "Hadrian",
			"last_name": "Ross",
			"phone_number": "6738975649",
			"address": "400 Brick st",
			"id": 9
		}
	]
}

Create a sale:
{
	"price": "787634",
	"automobile": "1234Gneyxnp7",
	"salesperson": "6",
	"customer": "8"
}
Sales list/returned body:
{
	"price": "787634",
	"id": 6,
	"customer": {
		"first_name": "Batman",
		"last_name": "Wayne",
		"phone_number": "8789876666",
		"address": "500 Clay St",
		"id": 8
	},
	"automobile": {
		"vin": "1234Gneyxnp7",
		"sold": true,
		"id": 11
	},
	"salesperson": {
		"first_name": "Frodo",
		"last_name": "Hubbard",
		"employee_id": "32",
		"id": 6
	}
}






## Services Ports and URLs backend
| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:8080/api/models/technicians/
| Create a technician | POST | http://localhost:8080/api/technicians/
| Delete a specific technician | DELETE | http://localhost:8080/api/technicians/:id/

| List appointments | GET | http://localhost:8080/api/appointments/
| Create an appointment | POST | http://localhost:8080/api/appointments/
| Delete an appointment | DELETE | http://localhost:8080/api/appointments/:id/
| Set appointment status to "canceled"  | PUT | http://localhost:8080/api/appointments/:id/cancel/
| Set appointment status to "finished" | PUT| http://localhost:8080/api/appointments/:id/finish/


## Services Ports and URLs frontend

| Action | Method | URL
| ----------- | ----------- | ----------- |
| List technicians | GET | http://localhost:3000/appointments/
| Create a technician | POST | http://localhost:3000/technicians/new

| List appointments | GET | http://localhost:3000/appointments
| Create an appointment | POST | http://localhost:3000/api/appointments/new
| Filter appointments by VIN  | PUT | http://localhost:3000/api/appointments/filtered/



## Service microservice

The service microservice has 3 models: Technician, AutomobileVO, and Appointment. The Appointment model interactas with the Technician model, receiving information from that model.

The AutomobileVO is a value object that gets data about the automobiles in the inventory using a poller. The service poller automatically polls the inventory microservice for data, so the service microservice is constantly getting the updated data.

These microservices are integrated because, when registering a service, a specific automobile needs to be selected, and that information lives inside the inventory microservice.

JSON GET technician data output:
```
	"technicians": [
		{
			"first_name": "Donny",
			"last_name": "Duck",
			"employee_id": 2,
			"id": 3
		},
		{
			"first_name": "Donny",
			"last_name": "Wahlberg",
			"employee_id": 3,
			"id": 4
		}
       ]
```

JSON POST technician data input:
```
  {
	"first_name": "Donald",
	"last_name": "Trump",
	"employee_id": 4
  }
`
JSON POST technician data output:
{
	"first_name": "Donald",
	"last_name": "Trump",
	"employee_id": 4,
	"id": 5
}

JSON GET appointment data output:

```
	"appointments": [
		{
			"date_time": "2021-07-11T00:00:00+00:00",
			"reason": "",
			"status": "CANCELED",
			"customer": "Damio",
			"technician": {
				"first_name": "Donny",
				"last_name": "Duck",
				"employee_id": 1,
				"id": 2
			},
			"vin": "1C3CC5FB2AN12017",
			"vip_status": false,
			"id": 6
		},
		{
			"date_time": "2021-07-11T00:00:00+00:00",
			"reason": "",
			"status": "FINISHED",
			"customer": "Damio",
			"technician": {
				"first_name": "Donny",
				"last_name": "Duck",
				"employee_id": 2,
				"id": 3
			},
			"vin": "1C3CC5FB2AN120174",
			"vip_status": false,
			"id": 8
		}
    ]
```

JSON POST appointment data input:

```
{
"vin": "1C3CC5FB2AN120174",
"customer": "Damien",
 "date_time": "2021-08-12T08:56:00+00",
 "technician": "44",
	"reason": "oil change"

}
```


JSON POST appointment data output:

```
{
	"date_time": "2021-08-12T08:56:00+00",
	"reason": "oil change",
	"status": "",
	"customer": "Damien",
	"technician": {
		"first_name": "Daniel",
		"last_name": "Gettinger",
		"employee_id": 44,
		"id": 6
	},
	"vin": "1C3CC5FB2AN120174",
	"vip_status": false,
	"id": 14
}
```
JSON PUT cancel appointment data output:
```
{
	"date_time": "2021-07-11T00:00:00+00:00",
	"reason": "",
	"status": "CANCELED",
	"customer": "Damio",
	"technician": {
		"first_name": "Donny",
		"last_name": "Duck",
		"employee_id": 2,
		"id": 3
	},
	"vin": "1C3CC5FB2AN120174",
	"vip_status": false,
	"id": 8
}
```
JSON PUT finish appointment data output:

```
{
	"date_time": "2021-07-11T00:00:00+00:00",
	"reason": "",
	"status": "FINISHED",
	"customer": "Damio",
	"technician": {
		"first_name": "Donny",
		"last_name": "Duck",
		"employee_id": 2,
		"id": 3
	},
	"vin": "1C3CC5FB2AN120174",
	"vip_status": false,
	"id": 8
}
```

## Sales microservice
On the backend, the sales microservice has 4 models: AutomobileVO, Customer, Salesperson, and Sale. Sale is the model that interacts with the other three models. This model gets data from the three other models.

The AutomobileVO is a value object that gets data about the automobiles in the inventory using a poller. The sales poller automotically polls the inventory microservice for data, so the sales microservice is constantly getting the updated data.

The reason for integration between these two microservices is that when recording a new sale, you'll need to choose which car is being sold and that information lives inside of the inventory microservice.

## Inventory microservice:
The Inventory API microservice enables the sales and service teams to pull Automobile information and
populate Automobile Value Object instances for each microservice to work with independently. This
microservice came fully packaged and neede 0 editing, and serves as the aggregate from which the other
two can pull down value objects.
