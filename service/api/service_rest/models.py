from django.db import models

# Create your models here.


class AutomobileVO(models.Model):
    vin = models.CharField(max_length=100)
    sold = models.BooleanField(default=False)


class Technician(models.Model):
    first_name = models.CharField(max_length=200)
    last_name = models.CharField(max_length=100)
    employee_id = models.SmallIntegerField(blank=True, null=True)


class Appointment(models.Model):
    date_time = models.DateTimeField(max_length=50, null=True, blank=True)
    reason = models.CharField(max_length=100)
    
    vin = models.CharField(max_length=100)
    customer = models.CharField(max_length=100)
    vip_status = models.BooleanField(default=False, null=True, blank=True)
    technician = models.ForeignKey(
        Technician,
        related_name="appointments",
        on_delete=models.CASCADE,
        null=True, blank=True
        )
    status = models.CharField(max_length=50)
    def __str__(self):
        return f"{self.technician} - {self.date_time}"

    # def get_api_url(self):
    #     return reverse("api_show_presentation", kwargs={"pk": self.pk})

    def finish(self):
        status = "FINISHED"
        self.status = status
        self.save()

    def cancel(self):
        status = "CANCELED"
        self.status = status
        self.save()

    class Meta():
        ordering = ("date_time", "technician")
