import React, {useState} from 'react';


function ManufacturerForm() {
    const [formData, setFormData] = useState({
    name: '',
  })


const [hasTechnician, setHasTechnician] = useState(false)
const handleSubmit = async (event) => {
    event.preventDefault();


const locationUrl = `http://localhost:8100/api/manufacturers/`;

const fetchConfig = {
    method: "post",
    body: JSON.stringify(formData),
    headers: {
      'Content-Type': 'application/json',
    },
  };

  
const response = await fetch(locationUrl, fetchConfig);

if (response.ok) {
  setFormData({
  name: '',
      });
      setHasTechnician(true);
    }
  }


const handleChangeName = (e) => {
const value = e.target.value;
const inputName = e.target.name;
  setFormData({
    ...formData,
    [inputName]: value
  });
}


const formClasses = (!hasTechnician) ? '' : 'd-none';
const messageClasses = (!hasTechnician) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

  return (
    <div className="my-5">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-manufacturer-form">
                <h1 className="card-title">Create Manufacturer</h1>
                <p className="mb-3">
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.name} required placeholder="name" type="text" id="name" name="name" className="form-control" />
                      <label htmlFor="model_name">first name</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create</button>
              </form>
              <div className={messageClasses} id="success-message">
                Manufacturer added!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default ManufacturerForm;
