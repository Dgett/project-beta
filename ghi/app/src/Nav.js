import { NavLink, Link } from 'react-router-dom';

function Nav() {
  return (
    <nav className="navbar navbar-expand-lg navbar-dark bg-success" style={{ backgroundImage: "linear-gradient(8120deg, #D8E0D6 0%, #CEA421 50%, #B97B63 50%)" }}>
      <div className="container-fluid">
        <NavLink className="navbar-brand fw-bold" to="/" >CarCar</NavLink>
        <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span className="navbar-toggler-icon"></span>
        </button>
        <div className="collapse navbar-collapse" id="navbarSupportedContent">
          <ul className="navbar-nav ms-auto mb-2 mb-lg-0">
            <li className="nav-item">
              <NavLink className="nav-link active me-2" aria-current="page" to="/">Home</NavLink>
            </li>
            <li className="nav-item dropdown me-2">
              <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <img src="https://cdn1.iconfinder.com/data/icons/ios-11-glyphs/30/car-512.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} /> Inventory
              </NavLink>
              <ul className="dropdown-menu dropdown-menu-end">
                <li>
                  <Link to="/manufacturers" className="dropdown-item">List manufacturers</Link>
                </li>
                <li>
                  <Link to="/vehicle_model_list" className="dropdown-item">Vehicle model list</Link>
                </li>
                <li>
                  <Link to="/manufacturers/new" className="dropdown-item">Create manufacturer</Link>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>
                <li>
                  <Link to="vehicle_model/new" className="dropdown-item">Create vehicle model</Link>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>
                <li>
                  <Link to="/automobiles" className="dropdown-item">List automobiles</Link>
                </li>
                <li>
                  <Link to="/automobiles/new/" className="dropdown-item">Create automobile</Link>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown me-2">
              <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <img src="https://uxwing.com/wp-content/themes/uxwing/download/e-commerce-currency-shopping/sale-icon.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} />Sales
              </NavLink>
              <ul className="dropdown-menu dropdown-menu-end">
                <li>
                  <Link to="salesperson/new" className="dropdown-item">Add a sales person</Link>
                </li>
                <li>
                  <Link to="customer/new" className="dropdown-item">Add a customer</Link>
                </li>
                <li>
                  <Link to="/sales/new" className="dropdown-item">Record a sale</Link>
                </li>
                <li>
                  <Link to="/salespersonlist/" className="dropdown-item">Salespeople list</Link>
                </li>
                <li>
                  <Link to="/customerlist/" className="dropdown-item">Customer list</Link>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>
                <li>
                  <Link to="/saleslist" className="dropdown-item">List all sales</Link>
                </li>
                <li>
                  <Link to="/sales/salesperson/history" className="dropdown-item">List sales by sales person</Link>
                </li>
              </ul>
            </li>
            <li className="nav-item dropdown">
              <NavLink className="nav-link dropdown-toggle" to="#" role="button" data-bs-toggle="dropdown" aria-expanded="false">
                <img src="https://www.iconpacks.net/icons/1/free-wrench-icon-951-thumb.png" className="pe-1 d-inline-block align-text-top" style={{ width: "25px" }} />Service
              </NavLink>
              <ul className="dropdown-menu dropdown-menu-end">
                <li>
                  <Link to="/technicians/new" className="dropdown-item">Create technician</Link>
                </li>
                <li>
                  <Link to="/appointments/new" className="dropdown-item">Create service appointment</Link>
                </li>
                <li>
                  <hr className="dropdown-divider" />
                </li>
                <li>
                  <Link to="/appointments" className="dropdown-item">List appointments</Link>
                </li>
                <li>
                  <Link to="/appointments/filtered" className="dropdown-item">List appointments by VIN</Link>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  )
}

export default Nav;
