import { useEffect, useState } from 'react';

function AutomobileList() {
  const [autos, setAutos] = useState([])

  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/automobiles/');

    if (response.ok) {
      const data = await response.json();
      setAutos(data.autos)
    }
  }

  useEffect(()=>{
    getData()
  }, [])


  return (
    <table className="table table-striped">
      <thead>
        <tr>
          <th>Year</th>
          <th>VIN</th>
          <th>sold</th>
          <th>color</th>
          <th>Model</th>
          <th>Make</th>
        </tr>
      </thead>
      <tbody className="table-group-dvider">
        {autos?.map((autos, idx) => {
          return (
            <tr key={autos.href}>
              <td>{ autos.year }</td>
              <td>{ autos.vin }</td>
              <td>{ JSON.stringify(autos.sold)}</td>
              <td>{ autos.color}</td>
              <td>{ autos.model.name}</td>
              <td>{ autos.model.manufacturer.name}</td>

            </tr>

          );
        })}
      </tbody>
    </table>
  );
}

export default AutomobileList;


