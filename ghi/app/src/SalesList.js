import { useEffect, useState } from 'react';

function SalesList() {
  const [selling, setSales] = useState([])
  const getData = async () => {
  const response = await fetch('http://localhost:8090/api/sales/');
        if (response.ok) {
            const data = await response.json();
            setSales(data.sales)
        }
    }

useEffect(()=>{
  getData()
  }, [])


return (
  <>
    <h1>Sales Records</h1>
    <table className="table table-striped">
    <thead>
      <tr>
        <th>Salesperson Employee ID</th>
        <th>Salesperson Name</th>
        <th>Customer</th>
        <th>VIN</th>
        <th>Price</th>
        </tr>
    </thead>
      <tbody>
        {selling.map((sales, idx) => {
          return (
            <tr>
              <td>{ sales.salesperson.employee_id }</td>
              <td>{ `${sales.salesperson.first_name} ${sales.salesperson.last_name}` }</td>
              <td>{ `${sales.customer.first_name} ${sales.customer.last_name}` }</td>
              <td> { sales.automobile.vin}  </td>
              <td> { sales.price} </td>
            </tr>
          );
        })}
      </tbody>
      </table>
  </>
  );
}

export default SalesList;
