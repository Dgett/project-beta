import { useEffect, useState } from 'react';


function ServiceHistory() {
  const [appointments, setAppointments] = useState([])
  const [filterValue, setFilterValue] = useState("");

  const getData = async () => {
    const response = await fetch('http://localhost:8080/api/appointments/');

    if (response.ok) {
      const data = await response.json();
      setAppointments(data.appointments)
    }
  }

  useEffect(()=>{
    getData()
  }, [])
  
function handleFilterChange(e) {
    console.log(e.target.value);
    setFilterValue(e.target.value);
}

const fetchAppointmentList = async () => {


    const listUrl = `http://localhost:8080/api/appointments/${appointments}/`
    const fetchList = await fetch(listUrl)

    if (fetchList.ok) {
        const appointmentData = await fetchList.json()
        setAppointments(appointmentData.appointments)
    }
}
let timeSettings = { timeZone: "UTC", year: "numeric", month: "numeric", day: "numeric", hour: "2-digit", minute: "2-digit" };
    
  return (
    
    <table className="table table-striped">
      <thead>
      <div className="col">
                    <div className="form-floating mb-3">
                    
                      {/* <label htmlFor="color">search by vin</label> */}
                      <button OnClick={handleFilterChange}className="btn btn-lg btn-primary">search</button>
                      <div className="form-floating mb-3"></div>
                      {/* <button className="btn btn-sm btn-warning text-white" onClick={fetchAppointmentList}>Search VIN</button> */}
                    </div>
                    <input onChange={handleFilterChange}  required placeholder="search by vin" type="text" id="search by vin" name="search by vin" className="form-control" />
                  </div>
        <tr> 
          <th>VIN</th>
          <th>VIP status</th>
          <th>Customer</th>
          <th>Reason</th>
          <th>Date&Time</th>
          <th>name</th>
          <th>status</th>
        </tr>
      </thead>
      <tbody>
        {appointments.filter(appointments => 
        appointments.vin.includes(filterValue))
        .map((appointment) => {
          return (
            <tr className={appointment.status === "CANCELED" ? "bg-danger-subtle" : appointment.status === "FINISHED" ? "bg-success-subtle" : null} key={appointment.id}>
              <td>{ appointment.vin}</td>
              <td className="text-warning">{JSON.stringify((appointment.vip_status ? "Yes" : null))}</td>
              <td>{ appointment.customer}</td>
              <td>{ appointment.reason }</td>
              <td>{new Date(appointment.date_time).toLocaleTimeString([], timeSettings)}</td>
              <td>{ appointment.technician.first_name } {appointment.technician.last_name}</td>
              <td>{ appointment.status}</td>
              
            </tr>
                );
           })}
      </tbody>
    </table>
    
        );
    }
export default ServiceHistory;
