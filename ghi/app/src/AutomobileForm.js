import React, {useState, useEffect} from 'react';


function AutomobileForm() {
  const [models, setModels] = useState([])
  const [formData, setFormData] = useState({
    color: '',
    year: '',
    vin: '',
    model:''

  })


  const [hasAutos, setHasAutos] = useState(false)
  const getData = async () => {
    const url = 'http://localhost:8100/api/models/';
    const response = await fetch(url);

    if (response.ok) {
      const data = await response.json();
      setModels(data.models);
    }
  }
  useEffect(() => {
    getData();
  }, []);


  const handleSubmit = async (event) => {
    event.preventDefault();

    const locationUrl = 'http://localhost:8100/api/automobiles/';

    const fetchConfig = {
      method: "post",
      body: JSON.stringify(formData),
      headers: {
        'Content-Type': 'application/json',
      },
    };

    const response = await fetch(locationUrl, fetchConfig);

    if (response.ok) {
      setFormData({
        color: '',
        year: '',
        vin: '',
        model: ''
      });

      setHasAutos(true);
    }
  }


  const handleChangeName = (e) => {
    const value = e.target.value;
    const inputName = e.target.name;
    setFormData({
      ...formData,
      [inputName]: value
    });
  }


   const formClasses = (!hasAutos) ? '' : 'd-none';
   const messageClasses = (!hasAutos) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

  return (
    <div className="my-5">
      <div className="row">
        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-automobile-form">
                <h1 className="card-title">Create Automobile</h1>
                <div className="mb-3">
                  <select onChange={handleChangeName} name="model_id" id="model_id" required>
                    <option value="">choose model</option>
                    {
                      models.map(model=> {
                        return (
                          <option key={model.id} value={model.id}> {model.name}</option>
                        )
                      })
                    }
                  </select>
                </div>
                <p className="mb-3">
                  Add an Automobile.
                </p>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.color} required placeholder="color" type="text" id="color" name="color" className="form-control" />
                      <label htmlFor="model_name">color</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.year} required placeholder="year" type="text" id="year" name="year" className="form-control" />
                      <label htmlFor="color">year</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.vin} required placeholder="vin" type="text"  id="vin" name="vin" className="form-control" />
                      <label htmlFor="picture_url">vin</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create</button>
              </form>
              <div className={messageClasses} id="success-message">
                Automobile added!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default AutomobileForm;
