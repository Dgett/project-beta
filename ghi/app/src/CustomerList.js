import { useEffect, useState } from 'react';


function CustomerList() {
  const [customers, setCustomers] = useState([])
  const getData = async () => {
  const response = await fetch('http://localhost:8090/api/customers/');

if (response.ok) {
    const data = await response.json();
    setCustomers(data.customer)
  }
}

useEffect(()=>{
  getData()
}, [])


return (
  <>
    <h1>Customers</h1>
      <table className="table table-striped">
        <thead>
          <tr>
            <th>First Name</th>
            <th>Last Name</th>
            <th>Phone Number</th>
            <th>Address</th>
          </tr>
        </thead>
        <tbody>
          {customers.map((customer, idx) => {
            return (
              <tr>
                <td>{ customer.first_name }</td>
                <td>{ customer.last_name }</td>
                <td>{ customer.phone_number}</td>
                <td>{ customer.address}</td>
              </tr>
            );
          })}
        </tbody>
      </table>
      </>
    );
  }

export default CustomerList;
