import React, {useState, useEffect } from 'react';


function VehicleModelForm() {
  const [manufacturerz, setManufacturer] = useState([])
  const [formData, setFormData] = useState({
        name: '',
        picture_url: '',
        manufacturer: '',
  })

const [hasVehicleModel, setHasVehicleModel] = useState(false)

const getData = async () => {
  const url = 'http://localhost:8100/api/manufacturers/'
  const response = await fetch(url);
    if (response.ok) {
      const data = await response.json();
      setManufacturer(data.manufacturers);
    }
  }


useEffect(() => {
  getData();
}, []);


const handleSubmit = async (event) => {
  event.preventDefault();
  const locationUrl = 'http://localhost:8100/api/models/';
  const fetchConfig = {
    method: "POST",
    body: JSON.stringify(formData),
    headers: {
      'Content-Type': 'application/json',
    },
  };
const response = await fetch(locationUrl, fetchConfig);
    if (response.ok) {
      setFormData({
        name: '',
        picture_url: '',
        manufacturer: '',
      });
      setHasVehicleModel(true);
    }
  }


const handleChangeName = (e) => {
  const value = e.target.value;
  const inputName = e.target.name;
  setFormData({
    ...formData,
    [inputName]: value
  });
}


const formClasses = (!hasVehicleModel) ? '' : 'd-none';
const messageClasses = (!hasVehicleModel) ? 'alert alert-success d-none mb-0' : 'alert alert-success mb-0';

  return (
    <div className="my-5">
      <div className="row">


        <div className="col">
          <div className="card shadow">
            <div className="card-body">
              <form className={formClasses} onSubmit={handleSubmit} id="create-vehicleModel-form">
                <h1 className="card-title">Create a vehicle model</h1>
                <p className="mb-3">
                  Please choose a Manufacturer
                </p>
                <div className="mb-3">
                  <select onChange={handleChangeName} name="manufacturer_id" id="manufacturer_id" required>
                    <option value="">Choose a manufacturer</option>
                    {
                      manufacturerz.map(manufacturers => {
                        return (
                          <option key={manufacturers.id} value={manufacturers.id}> {manufacturers.name}</option> //id?
                        )
                      })
                    }
                  </select>
                </div>
                <div className="row">
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.name} required placeholder="name" type="text" id="name" name="name" className="form-control" />
                      <label htmlFor="vin">Model Name</label>
                    </div>
                  </div>
                  <div className="col">
                    <div className="form-floating mb-3">
                      <input onChange={handleChangeName} value={formData.picture_url} required placeholder="picture_url" type="url" id="picture_url" name="picture_url" className="form-control" />
                      <label htmlFor="picture_url">Picture url</label>
                    </div>
                  </div>
                </div>
                <button className="btn btn-lg btn-primary">Create</button>
              </form>
              <div className={messageClasses} id="success-message">
                Model Created!
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}

export default VehicleModelForm;
