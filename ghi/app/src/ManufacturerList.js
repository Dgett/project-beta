import { useEffect, useState } from 'react';

function ManufacturerList() {
  const [manufacturers, setManufacturers] = useState([])
  const getData = async () => {
    const response = await fetch('http://localhost:8100/api/manufacturers/');
    if (response.ok) {
      const data = await response.json();
      setManufacturers(data.manufacturers)
    }
  }

useEffect(()=>{
  getData()
}, [])


return (
  <div className="container mt-5 pt-1">
    <div className="mt-5">
      <h1 className="text-center mb-3">Manufacturers</h1>
        <table className="table table-striped">
          <thead>
            <tr>
              <th>Name</th>
            </tr>
          </thead>
            <tbody>
          {manufacturers.map((manufacturer, idx) => {
              return (
                <tr key={manufacturer.id}>
                  <td>{ manufacturer.name }</td>
                </tr>
                );
              })}
            </tbody>
          </table>
      </div>
    </div>
  );
}

export default ManufacturerList;
